# matplotlib_intro.py
"""Introductory Labs: Intro to Matplotlib.
Logan Schelly
Math 345
15 Sept 2016
THIS FILE USES TABS INSTEAD OF 4 SPACES AS INDENTATION
"""
import numpy as np
from matplotlib import pyplot as plt

def var_of_means(n):
	"""Construct a random matrix A with values drawn from the standard normal
    distribution. Calculate the mean value of each row, then calculate the
    variance of these means. Return the variance.

    Inputs:
        n (int): The number of rows and columns in the matrix A.

    Returns:
        (float) The variance of the means of each row.
	"""
	#use np.random.randn() to create an nxn array of values sampled from
	#the standard normal distribution.
	A = np.random.randn(n,n)
	
	#calculate the mean of each row of the array
	means = np.mean(A, axis=1)
	
	#return the variance of these means
	return np.var(means)


def prob1():
	"""Create an array of the results of var_of_means() with inputs
	n = 100, 200, ..., 1000. Plot and show the resulting array.
	"""
	results = list()
	for i in range(100, 1100, 100):
		results += [var_of_means(i)]
	array_of_results = np.array(results)
	plt.plot(array_of_results)
	plt.show()
	return
	
def prob2():
	"""Plot the functions sin(x), cos(x), and arctan(x) on the domain
	[-2pi, 2pi]. Make sure the domain is refined enough to produce a figure
	with good resolution.
	"""
	#use np.pi for pi
	pi = np.pi
	#Make sure the domain is refined enough to produce a figure with good resolution.
	sufficient_resolution = 1000
	#Make the domain from -2pi to 2pi
	domain = np.linspace(-2*pi, 2*pi, sufficient_resolution)
	
	#sin(x)
	sin_range = np.sin(domain)
	plt.plot(domain, sin_range)
	plt.show()
	
	#cos(x)
	cos_range = np.cos(domain)
	plt.plot(domain, cos_range)
	plt.show()
	
	#arctan(x)
	arctan_range = np.arctan(domain)
	plt.plot(domain, arctan_range)
	plt.show()
	return


def prob3():
	"""Plot the curve f(x) = 1/(x-1) on the domain [-2,6].
        1. Split the domain so that the curve looks discontinuous.
        2. Plot both curves with a thick, dashed magenta line.
        3. Change the range of the y-axis to [-6,6].
	"""
	resolution = 1000
	domain = np.linspace(-2,6, resolution)
	
	#split the domain
	left_domain = domain[domain<1]
	right_domain = domain[domain>1]
	
	#plot the two sides of the curve separately
	#plot both curves with a thick, dashed magenta line
	plt.plot(left_domain, 1/(left_domain -1), 'm--', linewidth = 10)
	plt.plot(right_domain, 1/(right_domain -1), 'm--', linewidth = 10)
	
	#change to range of the y axis to [-6,6]
	plt.ylim(-6,6)
	
	plt.show()
	return


def prob4():
	"""Plot the functions sin(x), sin(2x), 2sin(x), and 2sin(2x) on the
    domain [0, 2pi].
        1. Arrange the plots in a square grid of four subplots.
        2. Set the limits of each subplot to [0, 2pi]x[-2, 2].
        3. Give each subplot an appropriate title.
        4. Give the overall figure a title.
        5. Use the following line colors and styles.
              sin(x): green solid line.
             sin(2x): red dashed line.
             2sin(x): blue dashed line.
            2sin(2x): magenta dotted line.
	"""
	#plot the functions of the domain [0, 2pi]
	x = np.linspace(0, 2*np.pi, 1000)
	
	#Arrange the plots in a square grid of 4 subplots
	r = rows = 2
	c = columns = 2
	
	#Give each subplot an appropriate titles
	titles = ["sin(x)", "sin(2x)", "2sin(x)", "2sin(2x"]
	
	#use the appropriate line color and style
	line_types = ['g-', 'r--', 'b--', 'm:']
	
	#Set the limits of each subplot to [0, 2pi]x[-2,2]
	for i in range(1,5,):
		plt.subplot(rows,columns,i)
		plt.axis([0, 2*np.pi, -2, 2])
		plt.title(titles[i-1])
		
		if i == 1:
			plt.plot(x, np.sin(x), line_types[i-1], lw = 2)
		elif i == 2:
			plt.plot(x, np.sin(2*x), line_types[i-1], lw = 2)
		elif i == 3:
			plt.plot(x, 2*np.sin(x), line_types[i-1], lw = 2)
		elif i == 4:
			plt.plot(x, 2*np.sin(2*x), line_types[i-1], lw = 2)
			
	#Give the figure an overall title with plt.suptitle()
	plt.suptitle("Stretching sin(x) by a factor of 2")
	plt.show()
	return


def prob5():
	"""Visualize the data in FARS.npy. Use np.load() to load the data, then
    create a single figure with two subplots:
        1. A scatter plot of longitudes against latitudes. Because of the
            large number of data points, use black pixel markers (use "k,"
            as the third argument to plt.plot()). Label both axes.
        2. A histogram of the hours of the day, with one bin per hour.
            Label and set the limits of the x-axis.
	"""
	#load the FARS data
	FARS = np.load("FARS.npy")
	
	#single figure with 2 subplots
	plt.subplot(1,2,1)
	

	#SCATTER PLOT
		
	#The longitude will be the second column of the FARS array
	longitude = FARS[:,1]
	#the latitude will be the 3rd column of the FARS array
	latitude = FARS[:,2]
	
	#plot longitude vs latitude with black pixel markers
	plt.plot(longitude, latitude, 'k,')
	
	#label both axes
	plt.xlabel("longitude")
	plt.ylabel("latitude")
	
	#Use plt.axis("equal") to fix the axis ratio on the scatter plot
	plt.axis("equal")
	
	#HISTOGRAM
	plt.subplot(1,2,2)
	#crash times are the first column
	crash_times = FARS[:,0]
	#crash times are just given as ain integer in military time
	plt.hist(crash_times, bins=24, range=[0,24])
	#set the limits of the x-axis
	plt.xlim(0,24)
	#label the x axis
	plt.xlabel("hour of day")
	
	
	
	#show the graph
	plt.show()
	return



def prob6():
	"""Plot the function f(x,y) = sin(x)sin(y)/xy on the domain
    [-2pi, 2pi]x[-2pi, 2pi].
        1. Create 2 subplots: one with a heat map of f, and one with a contour
            map of f. Choose an appropriate number of level curves, or specify
            the curves yourself.
        2. Set the limits of each subplot to [-2pi, 2pi]x[-2pi, 2pi].
	    3. Choose a non-default color scheme.
	    4. Add a colorbar to each subplot.
	"""
	# make the x domain
	x = np.linspace(-2*np.pi, 2*np.pi, 1000)
	#make y domain
	y = x.copy()
	#make the mesh grid
	X, Y = np.meshgrid(x,y)
	#make the range
	Z = (np.sin(X)*np.sin(Y))/(X*Y)
	
	#HEAT MAP
	#specify which subplot
	plt.subplot(121)
	#use the colormesh() function for a heat map, and use a non-default color
	plt.pcolormesh(X, Y, Z, cmap='magma')
	#set the limits
	plt.xlim(-2*np.pi ,2*np.pi)
	plt.ylim(-2*np.pi, 2*np.pi)
	#add a colorbar
	plt.colorbar()
	#give title
	plt.title("heat map of f(x,y)")
	
	#CONTOUR MAP
	#specify the subplot
	plt.subplot(122)
	#use the function contour() to make a countour plot
	plt.contour(X, Y , Z, 15, cmap="winter")
	#set the limits
	plt.xlim(-2*np.pi ,2*np.pi)
	plt.ylim(-2*np.pi, 2*np.pi)
	#add a colorbar
	plt.colorbar()
	#give title
	plt.title("countour map of f(x,y)")


	plt.suptitle("Problem 6: graphing sin(x)sin(y)/xy")
	#show the plot
	plt.show()
	return
	
if __name__ == "__main__":
	prob5()
