11/29/16 09:18

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
n_components function incorrect for 9.2
	Correct response: (2, 0.0)
	Student response: (2, 1.0000000000000002)
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
NotImplementedError: Problem 4 Incomplete

Total score: 25/40 = 62.5%

-------------------------------------------------------

12/06/16 08:44

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
n_components function incorrect for 9.2
	Correct response: (2, 0.0)
	Student response: (2, 1.0000000000000002)
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Positive of image incorrect
	Correct response: [[ 0.68104577  0.68235296  0.68758172 ...,  0.63137257  0.627451
   0.62352943]
 [ 0.69281048  0.69542485  0.69934636 ...,  0.64313728  0.63921571
   0.63660127]
 [ 0.70718956  0.70980388  0.71503264 ...,  0.65490198  0.6522876
   0.64575166]
 ..., 
 [ 0.          0.          0.         ...,  0.55032682  0.58039218
   0.57908499]
 [ 0.          0.          0.         ...,  0.55686277  0.58431369
   0.5777778 ]
 [ 0.          0.          0.         ...,  0.55032682  0.58562094
   0.5777778 ]]
	Student response: [[ True  True  True ...,  True  True  True]
 [ True  True  True ...,  True  True  True]
 [ True  True  True ...,  True  True  True]
 ..., 
 [False False False ...,  True  True  True]
 [False False False ...,  True  True  True]
 [False False False ...,  True  True  True]]
Negative of image incorrect
	Correct response: [[ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 ..., 
 [ 0.06013072  0.06405229  0.05098039 ...,  0.          0.          0.        ]
 [ 0.05882353  0.06666667  0.07058824 ...,  0.          0.          0.        ]
 [ 0.04444445  0.05098039  0.05098039 ...,  0.          0.          0.        ]]
	Student response: [[False False False ..., False False False]
 [False False False ..., False False False]
 [False False False ..., False False False]
 ..., 
 [ True  True  True ..., False False False]
 [ True  True  True ..., False False False]
 [ True  True  True ..., False False False]]
Score += 0

Total score: 25/40 = 62.5%

-------------------------------------------------------

12/07/16 08:24

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Positive of image incorrect
	Correct response: [[ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 ..., 
 [ 0.06013072  0.06405229  0.05098039 ...,  0.          0.          0.        ]
 [ 0.05882353  0.06666667  0.07058824 ...,  0.          0.          0.        ]
 [ 0.04444445  0.05098039  0.05098039 ...,  0.          0.          0.        ]]
	Student response: [[ 0.68104577  0.68235296  0.68758172 ...,  0.63137257  0.627451
   0.62352943]
 [ 0.69281048  0.69542485  0.69934636 ...,  0.64313728  0.63921571
   0.63660127]
 [ 0.70718956  0.70980388  0.71503264 ...,  0.65490198  0.6522876
   0.64575166]
 ..., 
 [ 0.          0.          0.         ...,  0.55032682  0.58039218
   0.57908499]
 [ 0.          0.          0.         ...,  0.55686277  0.58431369
   0.5777778 ]
 [ 0.          0.          0.         ...,  0.55032682  0.58562094
   0.5777778 ]]
Negative of image incorrect
	Correct response: [[ 0.68104577  0.68235296  0.68758172 ...,  0.63137257  0.627451
   0.62352943]
 [ 0.69281048  0.69542485  0.69934636 ...,  0.64313728  0.63921571
   0.63660127]
 [ 0.70718956  0.70980388  0.71503264 ...,  0.65490198  0.6522876
   0.64575166]
 ..., 
 [ 0.          0.          0.         ...,  0.55032682  0.58039218
   0.57908499]
 [ 0.          0.          0.         ...,  0.55686277  0.58431369
   0.5777778 ]
 [ 0.          0.          0.         ...,  0.55032682  0.58562094
   0.5777778 ]]
	Student response: [[ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 [ 0.          0.          0.         ...,  0.          0.          0.        ]
 ..., 
 [ 0.06013072  0.06405229  0.05098039 ...,  0.          0.          0.        ]
 [ 0.05882353  0.06666667  0.07058824 ...,  0.          0.          0.        ]
 [ 0.04444445  0.05098039  0.05098039 ...,  0.          0.          0.        ]]
Score += 0

Total score: 30/40 = 75.0%

-------------------------------------------------------

12/08/16 11:57

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Total score: 40/40 = 100.0%

Excellent!

-------------------------------------------------------

12/15/16 09:32

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Total score: 40/40 = 100.0%

Excellent!

-------------------------------------------------------

