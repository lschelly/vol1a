# qr_lstsq_eigs.py
"""Volume 1A: QR 2 (Least Squares and Computing Eigenvalues).
Logan Schelly
Math 321
29 October 2016"""

import numpy as np
from cmath import sqrt
from scipy import linalg as la
from matplotlib import pyplot as plt


# Problem 1
def least_squares(A, b):
	"""Calculate the least squares solutions to Ax = b using QR decomposition.

	Inputs:
		A ((m,n) ndarray): A matrix of rank n <= m.
		b ((m, ) ndarray): A vector of length m.

	Returns:
		x ((n, ) ndarray): The solution to the normal equation.
	"""
	#Get the reduced QR decomposition
	Q,R = la.qr(A, mode = "economic")
	#Solve the triangular system Rx = Q^T b
	x_hat = la.solve_triangular(R, np.dot(Q.T, b))
	return x_hat

# Problem 2
def line_fit():
	"""Load the data from housing.npy. Use least squares to calculate the line
	that best relates height to weight.

	Plot the original data points and the least squares line together.
	"""
	data = np.load("housing.npy")
	times = data[:,0] # times will be the x axis
	prices = data[:,1] # prices are the y axis
	
	# plot the original data points as a scatter plot
	plt.scatter(times+2000, prices, s=70, c = 'b', marker= 'p', label = "Price Data Points (also kinda look like houses)")
	
	#Get the least squares line mt + b = y
	times_as_column = (times.reshape((-1,1)))
	A = np.hstack((times_as_column, np.ones_like(times_as_column)))
	m, b = least_squares(A, prices)
	
	#Plot the least squares line with the scatter plot
	x = np.array((times[0], times[-1]))
	y = m*x + b
	plt.plot(x+2000,y, 'r-', label= "Least Squares Line")
	
	#Basic Plotting housekeeping
	plt.ylabel("Housing Price Index (relative to the index of 100 set in 1991)")
	plt.xlabel("Year of Our Lord")
	plt.legend(loc = "lower right")
	plt.axis('tight')
	plt.show()
	return


# Problem 3
def polynomial_fit():
	"""Load the data from housing.npy. Use least squares to calculate
	the polynomials of degree 3, 6, 9, and 12 that best fit the data.

	Plot the original data points and each least squares polynomial together
	in individual subplots.
	"""
	data = np.load("housing.npy")
	times = data[:,0] # times will be the x axis
	prices = data[:,1] # prices are the y axis
	
	refined_domain = np.linspace(min(times), max(times), 1000)
	
	
	degrees  = [3,6,9,12]
	subplots = [(2,2,1), (2,2,2), (2,2,3), (2,2,4)]
	for i in range(4):
		d = degrees[i]
		plt.subplot(subplots[i][0], subplots[i][1], subplots[i][2])
		
		#find the approximating polynomial
		#of degree d, we need d+1 columns (0th degree not counted)
		A = np.vander(times, d+1)
		polynomial_coefficients = la.lstsq(A,prices)[0]
		fitting_polynomial = np.poly1d(polynomial_coefficients)
		
		#graph the approximating polynomial on the more refined domain
		plt.plot(refined_domain + 2000 , fitting_polynomial(refined_domain), 'r-', label = "Fitting Polynomial")
		
		#graph the data points as a scatter plot
		plt.scatter(times + 2000, prices, c='b', marker='p', label="Housing Price")
		
		#Basic Plotting housekeeping
		plt.title("Degree %d Polynomial"%d)
		plt.ylabel("Housing Price Index")
		plt.xlabel("Year")
		plt.legend(loc = "lower right")
		plt.axis('tight')
	
	plt.suptitle("Polynomial Fitting")	
	plt.show()
	return


def plot_ellipse(a, b, c, d, e):
	"""Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
	theta = np.linspace(0, 2*np.pi, 200)
	cos_t, sin_t = np.cos(theta), np.sin(theta)
	A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
	B = b*cos_t + d*sin_t
	r = (-B + np.sqrt(B**2 + 4*A))/(2*A)

	plt.plot(r*cos_t, r*sin_t, lw=2)
	plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
	"""Load the data from ellipse.npy. Use least squares to calculate the
	ellipse that best fits the data.

	Plot the original data points and the least squares ellipse together.
	"""
	ellipse = np.load("ellipse.npy")
	x_points = ellipse[:,0]
	y_points = ellipse[:,1]
	
	#plot the original data points
	plt.scatter(x_points, y_points, marker = '*', c = 'k', label = "data points")
		
	#Construct the matrix
	A = np.vstack((x_points**2, x_points, x_points*y_points, y_points, y_points**2))
	A = A.T
	
	#Need ax**2 + bx + cxy + dy + ey**2 = 1
	#get the a,b,c,d, and e for the ellipse
	a,b,c,d,e = la.lstsq(A,np.ones_like(x_points))[0]
	
	plot_ellipse(a,b,c,d,e)
	
	plt.axis('tight')
	plt.show()
	return


# Problem 5
def power_method(A, N=20, tol=1e-12):
	"""Compute the dominant eigenvalue of A and a corresponding eigenvector
	via the power method.

	Inputs:
		A ((n,n) ndarray): A square matrix.
		N (int): The maximum number of iterations.
		tol (float): The stopping tolerance.

	Returns:
		(foat): The dominant eigenvalue of A.
		((n, ) ndarray): An eigenvector corresponding to the dominant
			eigenvalue of A.
	"""
	m,n = A.shape
	if m!= n:
		raise ValueError("%r is not a square matrix"%A)
	x_k = np.random.random(n) #make a random first guess
	x_k /= la.norm(x_k) #normalize x
	for throwaway_index in range(N):#iterate N times
		x_kp1 = np.dot(A, x_k)#next guess is previous multiplied by A
		x_kp1 /= la.norm(x_kp1)#normalized
		if la.norm(x_k - x_kp1) < tol:
			x_k = x_kp1 #update
			break #done with algorithm. exit
		else:
			x_k = x_kp1
			continue #keep doing the algorithm
	# Ax = lambda*x
	# np.dot(x,x) = 1
	# np.dot(x, Ax)
	#=np.dot(x, lambda x)
	#=lambda np.dot(x,x)
	#= lambda*1
	#= lambda
	#So, np.dot(x, np.dot(A, x)) = lambda
	return np.dot(x_k, np.dot(A, x_k)), x_k

# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
	"""Compute the eigenvalues of A via the QR algorithm.

	Inputs:
		A ((n,n) ndarray): A square matrix.
		N (int): The number of iterations to run the QR algorithm.
		tol (float): The threshold value for determining if a diagonal block
			is 1x1 or 2x2.

	Returns:
		((n, ) ndarray): The eigenvalues of A.
	"""
	m,n = A.shape
	S = la.hessenberg(A)
	for k in range(N):
		Q,R = la.qr(S)
		S = np.dot(R,Q)
	eigs = list()
	i = 0
	while(i<n):
		# Si_is_last_diagonal_entry_of_S = (i == n-1)
		
		# The absolute value of element below the 
		#ith main diagonal entry of S (the lower left element of the 2x2 block)
		#is less than tol = 
		# S[i+1, i] < tol
		if (i == n-1) or (S[i+1, i] < tol):
			eigs.append(S[i,i])
			i += 1
		else:
			#Calculate the eigenvalues of Si
			#a = S[i,i]
			#b = S[i,i+1]
			#c = S[i+1, i]
			#d = S[i+1, i+1]
			a,b,c,d = S[i:i+2, i:i+2].ravel()
			#lambda**2 - (a+d)*lambda + (a*d-b*c) = 0
			#Use the quatratic formula
			# lambda = ((a+d) +/- sqrt((a+d)**2 - 4*(a*d-b*c))))/2
			eigenvalue = (a + d + sqrt((a+d)**2 - 4*(a*d-b*c)))/2
			eigs += [eigenvalue, eigenvalue.conjugate()]
			i += 2
	
	return np.array(eigs)

if __name__ == "__main__":
	want_to_test_prob1 = False
	want_to_test_prob2 = False
	want_to_test_prob3 = False
	want_to_test_prob4 = False
	want_to_test_prob5 = False
	want_to_test_prob6 = True
	
	if want_to_test_prob1:
		for m in range(2,7)[::-1]:
			for n in range(1, m+1):
				for dummy_index in range(5):
					A = np.random.random((m,n))
					b = np.random.random(m)
					
					print "This is A:"
					print A
					print "\nThis is b:"
					print b.reshape((-1,1))
					
					myx0 = least_squares(A,b)
					print "\nThis is my x0:"
					print myx0.reshape((-1,1))
					
					x0 = np.linalg.lstsq(A,b)[0]
					print "\nThis is Numpy's x0:"
					print x0.reshape((-1,1))
					
					if myx0.all() == x0.all():
						print "They're exactly the same"
					elif np.allclose(myx0, x0):
						print "They're close enough"
					else:
						print "They're not close enough"
						raw_input("Press enter to continue")
					
					print "\n"
			
	if want_to_test_prob2:
		line_fit()
		
	if want_to_test_prob3:
		polynomial_fit()
	
	if want_to_test_prob4:
		ellipse_fit()
	
	if want_to_test_prob5:
		for n in range(1,13):
			for throwaway_index in range(5):
				A = np.random.random((n,n))
				mylambda, myeigenvector = power_method(A)
				nplambdas, npeigenvectors = la.eig(A)
				#the largest eigenvector/value is always first in the array
				nplambda = nplambdas[0]
				npeigenvector = npeigenvectors[0]
				print "My eigenvalue:		", mylambda
				print "Python's eigenvalue:	", nplambda
				print "My eigenvector next to Python's eigenvector:"
				whatever = np.vstack((myeigenvector, npeigenvector))
				print whatever.T
	
	if want_to_test_prob6:
		for n in range(2,6):
			print "\n\n\tMatrices of size %d"%n
			for throwaway_index in range(10):
				A = np.random.random((n,n))
				A += A.T
				print "My eigenvalues:"
				print qr_algorithm(A)
				print "la.eig's eigenvalues:"
				print la.eig(A)[0]
				print
