#python_intro.py
"""This is the introductory assignment

It can print hello world

It can compute the volume of a sphere
given a radius

It can print the first half of a string parameter, and it can reverse a string parameter

It does a function that tests whether or not I can do some list operations

It can translate a word in english to a word in pig latin

It has a function that finds the largest palindrome that is the product of two 3-digit numbers

It can compute the 500,000th partial sum of the alternating harmonic series

Created: 1 Sept 2016
Author: Logan Schelly"""


#Volume of sphere function (problem 2)
def sphere_volume(r):
    pi = 3.14159
    return 4.0/3.0*pi*r**3

#First half (problem 3)
def first_half(x):
    return x[ 0 : len(x)/2 : 1 ]

#Reverse (problem 3)
def backward(x):
    return x[len(x): : -1]

#List operations
def list_ops():
    animal_list = [ "bear", "dog", "ant", "cat"]
    
    #append "eagle"
    animal_list.append("eagle")

    #Replace the entry at index 2 with "fox".
    animal_list.pop(2)
    animal_list.insert(2, "fox")

    #Remove the entry at index 1
    animal_list.pop(1)

    #Sort the list in reverse alphabetical order
    animal_list.sort()
    animal_list = backward(animal_list)

    return animal_list

#pig latin translator
def pig_latin(word):
    if (word[0] in {'a', 'e', 'i', 'o', 'u', 'y'}):
	    return word + "hay"
    else:
        return word[1:] + word[0] + "ay"
        
#palindrome
""" Finds the largest palindrome that is the product
of two three digit natural numbers"""
palindrome_dict = {121: (11,11)}
def palindrome():
    palindromes = set()
    for i in range(100,1000,1):
        for j in range(i,1000,1):
            if str(i*j) == backward(str(i*j)):
                palindromes.add(i*j)
                palindrome_dict[i*j] = (i,j)
    return max(palindromes)
     
#alternating harmonic series
def alt_harmonic(N):
    return sum( (-1)**(n+1) * 1.0/n for n in range(N, 0, -1) )
        
        
#Hello world and sphere volume example (problems 1 & 2)
if __name__ == "__main__":
    print("Hello, world!")
    print sphere_volume(1)
    print first_half("123456789")
    print backward("123456789")
    print (list_ops())
