# svd_image_compression.py
"""Volume 1A: SVD and Image Compression.
Logan Schelly
Math 321
1 December 2016
"""

from scipy import linalg as la
import numpy as np
from matplotlib import pyplot as plt

# Problem 1
def truncated_svd(A,k=None):
	"""Computes the truncated SVD of A. If r is None or equals the number
		of nonzero singular values, it is the compact SVD.
	Parameters:
		A: the matrix
		k: the number of singular values to use
	Returns:
		U - the matrix U in the SVD
		s - the diagonals of Sigma in the SVD
		Vh - the matrix V^H in the SVD
	"""
	#Compute A^H * A
	AHA = np.dot(A.conjugate().T , A)
	
	"""
	#------------Discarded because it doesn't give same answer, though it is correct
	#Find Eigenvalues and eigenvectors of A^H * A
	#eigh returns them in ascending order by default
	#evals, evecs = la.eigh(AHA)
	#----------------------------------
	"""
	#-------------This is the way that does it without eigh
	#Find eigenvalues and eigenvectors using the eig command
	evals, evecs = la.eig(AHA)
	
	#sort them
	evecs = evecs[:,np.argsort(evals)]
	evals = evals[np.argsort(evals)] #sort the columns
	
	#cast to real
	evecs = np.real(evecs)
	evals = np.real(evals)
	#-------------------------------------------------------

	#Discard the zero eigenvalues/vectors
	evecs = evecs[:,~np.isclose(0,evals)] #eigh returns the eigenvectors as columns
	evals = evals[~np.isclose(0,evals)]
	
	#We need them both in descending order for svd
	evals = evals[::-1]
	evecs = evecs[:,::-1]#reverse columns of eigenvectors matrix

	#Keep only the k largest eigenvalues
	if k != None:
		evecs = evecs[:,:k]#keep the first k columns of the eigenvectors
		evals = evals[:k]
	
	#find the singular values of A
	s = np.sqrt(evals)
	
	#Caluculate V
	#eigh returns the vectors already normalized
	#evecs already show up as columns and in a matrix.  Make them V
	V = evecs
	Vh = V.conjugate().T
	
	#Calculate U
	U = np.dot(A,V) / s #s will broadcast the singluar values through

	return U, s, Vh

# Problem 2
def visualize_svd():
	"""Plot each transformation associated with the SVD of A."""
	#Make the unit circle
	theta = np.linspace(0, 2 * np.pi, 1000)
	x = np.cos(theta)
	y = np.sin(theta)
	S = np.vstack((x,y))
	
	#Plot the unit circle
	plt.subplot(2,2,1)
	plt.plot(S[0], S[1],'b-', label = "unit circle")
	
	#Plot the standard basis vectors
	#e1
	e1 = np.array([
			[0,1],
			[0,0]])
	plt.plot(e1[0],e1[1],'g-', label = "e1")
	
	#e2
	e2 = np.array([
			[0,0],
			[0,1]])
	plt.plot(e2[0],e2[1],'r-', label = "e2")
	
	#housekeeping
	plt.title("Preimage")
	plt.legend(loc = "upper right", prop={'size':8})		
	plt.axis("equal")
	
	#Get the SVD of A
	A = np.array([
		[3, 1],
		[1, 3]])
	U, s, Vh = truncated_svd(A)
	Sigma = np.diag(s)
	
	#Plot it under the transformation Vh
	S = np.dot(Vh, S)
	e1 = np.dot(Vh, e1)
	e2 = np.dot(Vh, e2)
	plt.subplot(2,2,2)
	plt.plot(S[0],S[1],'b-',label = "unit circle")
	plt.plot(e1[0],e1[1],'g-',label = "e1")
	plt.plot(e2[0],e2[1],'r-',label = "e2")
	plt.legend(loc = "upper right", prop={"size":8})
	plt.axis("equal")
	plt.title("Image Under V.H")
	
	#Now plot it under the transformation Sigma*Vh
	S = np.dot(Sigma, S)
	e1 = np.dot(Sigma, e1)
	e2 = np.dot(Sigma, e2)
	plt.subplot(2,2,3)
	plt.plot(S[0],S[1],'b-',label = "unit circle")
	plt.plot(e1[0],e1[1],'g-',label = "e1")
	plt.plot(e2[0],e2[1],'r-',label = "e2")
	plt.legend(loc = "upper right", prop={"size":8})
	plt.axis("equal")
	plt.title("Image Under Sigma * V.H")
	
	#Now plot it under the transformation U * Sigma * Vh
	S = np.dot(U, S)
	e1 = np.dot(U, e1)
	e2 = np.dot(U, e2)
	plt.subplot(2,2,4)
	plt.plot(S[0],S[1],'b-', label = "unit circle")
	plt.plot(e1[0],e1[1],'g-',label = "e1")
	plt.plot(e2[0],e2[1],'r-', label = "e2")
	plt.legend(loc = "upper right", prop={"size":8})
	plt.axis("equal")
	plt.title("Image Under A")
	
	plt.suptitle("Problem 2")
	plt.show()
	
	return

# Problem 3
def svd_approx(A, k):
	"""Returns best rank k approximation to A with respect to the induced 2-norm.

	Inputs:
	A - np.ndarray of size mxn
	k - rank

	Return:
	Ahat - the best rank k approximation
	"""
	U, s, Vh = la.svd(A, full_matrices = False)
	
	#Keep the first k singular values
	Sigma = np.diag(s[:k])
	
	#Keep the first k columns of U
	U = U[:,:k]
	
	#Keep the first k rows of Vh
	Vh = Vh[:k,:]
	
	#Ahat = np.dot(U, np.dot(Sigma, Vh))
	#Return Ahat
	return np.dot(U, np.dot(Sigma, Vh))

# Problem 4
def lowest_rank_approx(A,e):
	"""Returns the lowest rank approximation of A with error less than e
	with respect to the induced 2-norm.

	Inputs:
	A - np.ndarray of size mxn
	e - error

	Return:
	Ahat - the lowest rank approximation of A with error less than e.
	"""
	U, s, Vh = la.svd(A, full_matrices = False)
	
	#check to see if any of the values are less than e
	if np.any(s < e):
		#find the index of the first singular value smaller than the error
		k = np.argmax(s < e)

		#retain the parts of the SVD that have singular values greater than e
		s = s[:k]
		U = U[:,:k]
		Vh = Vh[:k,:]
	
	#Even if none of the singular values are less than e, still define Sigma
	Sigma = np.diag(s)
	
	return np.dot(U, np.dot(Sigma, Vh))

# Problem 5
def compress_image(filename,k):
	"""Plot the original image found at 'filename' and the rank k approximation
	of the image found at 'filename.'

	filename - jpg image file path
	k - rank
	"""
	X = plt.imread(filename)
	#give the matrix entries values between 0 and 1
	X = X/255.
	#plot the original image
	plt.suptitle("Problem 5 -- Image Compression")
	plt.subplot(1,2,1)
	plt.title("Original Image")
	plt.imshow(X)
	
	#Do the SVD of the 3 layers
	R = svd_approx(X[:,:,0], k)
	G = svd_approx(X[:,:,1], k)
	B = svd_approx(X[:,:,2], k)
	
	#Set entries greater than 1 to 1
	R[R>1] = 1
	G[G>1] = 1
	B[B>1] = 1
	
	#Set entries less than 0 to 0
	R[R<0] = 0
	G[G<0] = 0
	B[B<0] = 0
	
	#Re-combine RGB
	X_compressed = np.zeros_like(X)
	X_compressed[:,:,0] = R
	X_compressed[:,:,1] = G
	X_compressed[:,:,2] = B
	
	#plot the compressed image
	plt.subplot(1,2,2)
	plt.title("Compressed Image -- rank %d"%k)
	plt.imshow(X_compressed)
	plt.show()
	
	return

#Testing
	
if __name__ == "__main__":
	want_to_test_problem_1 = True
	want_to_test_problem_2 = False
	want_to_test_problem_3 = False
	want_to_test_problem_4 = False
	want_to_test_problem_5 = False
	
	
	if want_to_test_problem_1:
		def svd_tester(matrix):
			"""
			function for testing the SVD function
			"""
			print "This is the matrix:"
			print matrix
			print
			U, s, Vh = truncated_svd(matrix)
			Sigma = np.diag(s)
			SVD_matrix = np.dot(U, np.dot(Sigma, Vh) )
			print "This is what happens when you multiply out the SVD:"
			print SVD_matrix
			if np.allclose(SVD_matrix, matrix):
				print "They're close enough"
			else:
				raw_input("That's not close enough.")
			print "\n"
		
		grind_through_a_bunch_of_square_matrices = False
		grind_through_a_bunch_of_4x5s = False
		grind_through_a_bunch_of_different_sized = False
		number_to_grind = 100
		
		#just test this one
		A = np.array([
		[ 0.10018117,  0.64485315,  0.08826282,  0.89609481,  0.80197722],
		[ 0.03027495,  0.84035316,  0.21240662,  0.1491444 ,  0.13032778],
		[ 0.43634228,  0.25996003,  0.60306148,  0.71989767,  0.07361437],
		[ 0.51981252,  0.58253232,  0.540624  ,  0.30552332,  0.33458147]])
		svd_tester(A)
		
		#Test the A they will use
		A = np.array([[1,0,0,0,2],[0,0,3,0,0],[0,0,0,0,0],[0,2,0,0,0]])
		U, s, Vh = truncated_svd(A)
		print "This is the matrix they test:"
		print A
		print
		print "This is U:"
		print U
		print 
		print "This is Vh:"
		print Vh
		print
		
		#squares	
		if grind_through_a_bunch_of_square_matrices:	
			for i in range(3,10):
				for throwaway_index in range(number_to_grind):
					print
					A= np.random.random((i,i))
					svd_tester(A)
		#4x5's
		if grind_through_a_bunch_of_4x5s:
			for i in range(number_to_grind):
				A = np.random.random((4,5))		
				svd_tester(A)
				
				"""
				#problem matrix
				A = 
				[[ 0.87193802  0.11096655  0.49569323  0.80339694  0.25580072]
 [ 0.70375331  0.52529855  0.41947529  0.46234551  0.28985168]
 [ 0.19152855  0.09095121  0.04642179  0.68602102  0.88104717]
 [ 0.05982524  0.67142969  0.02113426  0.60459292  0.51529379]]

				A = np.random.random((5,4))
				print "A:"
				print A
				U, s, Vh = truncated_svd(A)
				Sigma = np.diag(s)
				A_SVD = np.dot(U, np.dot(Sigma, Vh))
				print "SVD result:"
				print A_SVD
				if np.allclose(A, A_SVD):
					print "Close enough."
				else:
					raw_input("Those two aren't close enough.  Press enter to continue")
				"""
		#Varying_sizes
		if grind_through_a_bunch_of_different_sized:
			for m in range(1,100):
				n = 100 - m
				for throwaway in range(number_to_grind):
					A = np.random.random((m,n))
					svd_tester(A)
				
				
	if want_to_test_problem_2:
		visualize_svd()
		
	if want_to_test_problem_3:
		A= np.array([[26, 86, 57, 52, 63,  6, 63],
		[61, 41, 72, 19, 86, 34, 27],
		[40, 33, 21, 30, 79,  7, 23],
		[87, 26, 74, 45, 63, 41, 98],
		[ 7, 12, 92, 23, 24, 95,  9],
		[90, 46,  7,  9, 14, 45, 14],
		[98, 19, 32, 80,  4,  0, 31]])

		print "A:"
		print A
		print "singular values:"
		sigmas = la.svd(A)[1]
		print sigmas

		for k in range(9)[::-1]:
			print "\nRank %d approximation of A"%k
			A_k = svd_approx(A,k)
			print A_k
			print "Norm of A_k - A:", la.norm(A-A_k, ord=2)
			try:
				print "sigma _ k+1 = ", sigmas[k]
			except IndexError:
				print "sigma _ k+1 = ", sigmas[6]
				
	if want_to_test_problem_4:
		A= np.array([
		[26, 86, 57, 52, 63,  6, 63],
		[61, 41, 72, 19, 86, 34, 27],
		[40, 33, 21, 30, 79,  7, 23],
		[87, 26, 74, 45, 63, 41, 98],
		[ 7, 12, 92, 23, 24, 95,  9],
		[90, 46,  7,  9, 14, 45, 14],
		[98, 19, 32, 80,  4,  0, 31]])
		
		for e in range(0,500,50):
			print "Error less than %d approximation"%e
			A_e = lowest_rank_approx(A, e)
			print A_e
			print "2-norm of (approx - A)", la.norm(A - A_e, ord = 2)			
		
		
	if want_to_test_problem_5:
		image = "hubble_image.jpg" # also fun -- "twenty_one_pilots_symbol.jpg"
		rank = int(raw_input("What rank would you like?"))
		#compress_image("twenty_one_pilots_symbol.jpg", rank)
		compress_image(image, rank)
		for rank in [2,4,8,16,32,64,128]:
			#compress_image("twenty_one_pilots_symbol.jpg", rank)
			compress_image(image, rank)
