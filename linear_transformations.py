# linear_transformations.py
"""Volume 1A: Linear Transformations.
Logan Schelly
Math 345
23 September 2016
"""
#use pylplot for pretty graphs
from matplotlib import pyplot as plt

#use random because rolling dice repeatedly gets tedious
from random import random

#import numpy because it's awesome.
import numpy as np

#I want to time things, and I don't want to use my stopwatch
from timeit import timeit

def check_transformation_inputs(A,a,b=0.0):
    """
    checks to make sure the inputs are valid for the transformations
    """
    
    #a
    if(isinstance(a, float) or isinstance(a, int)):
        pass
    else:
        raise TypeError("%r must be an int or float.  It is a %s" %(a, type(a)))
        
    #b    
    if(isinstance(b, float) or isinstance(b, int)):
        pass
    else:
        raise TypeError("%r must be an int or float.  It is a %s" %(b, type(b)))
        
    #A must be a 2xanything matrix
    if isinstance(A, np.ndarray):
        if np.ndim(A) == 2:
            if A.shape[0] == 2:
                pass
            else: # did not have two rows
                raise ValueError("%r must have 2 rows.  It has %i" %(A, A.shape[0]))
        else: # not 2 dimensional (not a matrix)
            raise ValueError("%r must have 2 axes.  It has %i" %(A, ndim(A)))
    else: #not array
        raise TypeError("%r must be an numpy.ndarray.  It is a %s" % (A, type(A)))
    return
    #----------------------------------


# Problem 1
def stretch(A, a, b):
    """Scale the points in 'A' by 'a' in the x direction and 'b' in the
    y direction.

    Inputs:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    check_transformation_inputs(A,a,b)
    
    #force b and a to be upcast to floats
    bf = float(b)
    af = float(a)
    #I will use af and bf for the rest of the function
    
    # Make the following matrix:
    """
    |a 0|
    |0 b|
    """
    #--------------------------------------
    Stretch_Matrix = np.array([[af,0 ],
                               [0 ,bf]])
    #--------------------------------------
    # Return the restulting transformation
    #--------------------------------------
    return np.dot(Stretch_Matrix, A)
                                     


def shear(A, a, b):
    """Slant the points in 'A' by 'a' in the x direction and 'b' in the
    y direction.

    Inputs:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    check_transformation_inputs(A,a,b)
    
    #force b and a to be upcast to floats
    bf = float(b)
    af = float(a)
    #I will use af and bf for the rest of the function
    
    #make the following matrix
    """
    |1 a|
    |b 1|
    *Technically this isn't a composition of shears.
    It shears the x data and the y data at the same time
    """
    Shear_Matrix = np.array([[1,af],
                             [bf,1]])
    #Return the resulting transformation
    return np.dot(Shear_Matrix, A)

def reflect(A, a, b):
    """Reflect the points in 'A' about the line that passes through the origin
    and the point (a,b).

    Inputs:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): x-coordinate of a point on the reflecting line.
        b (float): y-coordinate of the same point on the reflecting line.
    """
    check_transformation_inputs(A,a,b)
    
    #force b and a to be upcast to floats
    bf = float(b)
    af = float(a)
    #I will use af and bf for the rest of the function
    
    #make this matrix:
    """
        1         |a^2-b^2    2ab  |
    --------- *  |                |
    a^2 + b^2    |2ab      b^2-a^2|
    """
    Reflection_Matrix = (1.0/(af**2 + bf**2))*np.array([[af**2 - bf**2, 2*af*bf],
                                                        [2*af*bf, bf**2 - af**2]])
    #return the resulting transformation
    return np.dot(Reflection_Matrix, A)
    
    raise NotImplementedError("Problem 1 Incomplete")

def rotate(A, theta):
    """Rotate the points in 'A' about the origin by 'theta' radians.

    Inputs:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        theta (float): The rotation angle in radians.
    """    
    check_transformation_inputs(A,theta)
    
    #force theta to be a float
    thetaf = float(theta)
    #I will use theta f for the rest of the function
    
    #make this matrix:
    """
    |cos(theta)        -sin(theta)|
    |sin(theta)    cos(theta)|
    """
    Rotation_Matrix = np.array([[np.cos(thetaf), -np.sin(thetaf)],
                                [np.sin(thetaf), np.cos(thetaf)]])
    #return the transformation
    return np.dot(Rotation_Matrix, A)


# Problem 2
def solar_system(T, omega_e, omega_m):
    """Plot the trajectories of the earth and moon over the time interval [0,T]
    assuming the initial position of the earth is (10,0) and the initial
    position of the moon is (11,0).

    Parameters:
        T (int): The final time.
        omega_e (float): The earth's angular velocity.
        omega_m (float): The moon's angular velocity.
    """
    #------------------------------------------------
    #FIND THE DATA
    #------------------------------------------------
    
    earth_original_position = eop = np.array([[10,0]]).T
    moon_original_position = mop = np.array([[11,0]]).T
    moon_original_position_relative_to_earth = mope = mop - eop
    
    n = 1000
    #Generate a 2xn matrix with the earth's positions at n 
    #evenly spaced times in the interval between 0 and T.
    earth_positions = earth_original_position
    times = np.linspace(0,T,n)
    for t in times[1::]:
        #calculate the position at time t
        position_at_t = rotate(eop, t*omega_e)
        #augment that position to the right side of the positions matrix
        earth_positions = np.hstack((earth_positions, position_at_t))
    
    #Generate a 2xn matrix with the moon's positions 
    #relative to the earth at n evenly spaced times
    relative_moon_positions = mope
    for t in times[1::]:
        #calculate the position relative to the earth at time t
        position_at_t = rotate(mope, t*omega_m)
        #augment that position to the right side of the matrix
        relative_moon_positions = np.hstack((relative_moon_positions, position_at_t))
    
    #Now we need to get the positions of the moon relative to the sun
    #This should be sum of the relative moon matrix and the earth matrix
    moon_positions = earth_positions + relative_moon_positions
    
    #-----------------------------------------------------------
    #PLOT THE DATA
    #-----------------------------------------------------------    
    #Draw the trajectory of the earth as a blue line
    earth_x = earth_positions[0]
    earth_y = earth_positions[1]
    plt.plot(earth_x, earth_y, 'b-', linewidth = 4, label = "Earth")
    
    #Draw the trajectory of the moon as a white line
    moon_x = moon_positions[0]
    moon_y = moon_positions[1]
    plt.plot(moon_x, moon_y, 'w-', linewidth = 1, label = "Moon")
    
    #----------------------------------------------------------
    #MAKE THE GRAPH PRETTY
    #----------------------------------------------------------
    
    #fix the aspect ratio
    plt.gca().set_aspect("equal")
    
    #make the background black, because it's in space
    plt.gca().set_axis_bgcolor('black')

    #put a couple stars
    stars = (np.random.rand(2, np.random.randint(50,100))-.5)*30
    plt.plot(stars[0], stars[1], 'y,', label = "Stars")
        
    #give a legend so people know what the heck they're looking at
    #change the legend background color so we can see the moon
    plt.legend(loc = "upper left",prop={'size':12}).get_frame().set_facecolor('grey')
    
    #put a snarky title
    plt.title("\"Epicycles Are So Last Millenium\" - Galileo")
    
    #display the graph
    plt.show()
    return




def random_vector(n):
    """Generate a random vector of length n as a list."""
    return [random() for i in xrange(n)]

random_vector_raw_source = """
def random_vector(n):
    return [random() for i in xrange(n)]
    
"""

def random_matrix(n):
    """Generate a random nxn matrix as a list of lists."""
    return [[random() for j in xrange(n)] for i in xrange(n)]

random_matrix_raw_source = """
def random_matrix(n):
    return [[random() for j in xrange(n)] for i in xrange(n)]
    
"""

def matrix_vector_product(A, x):
    """Compute the matrix-vector product Ax as a list."""
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]

matrix_vector_product_raw_source = """
def matrix_vector_product(A, x):
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]
    
"""

def matrix_matrix_product(A, B):
    """Compute the matrix-matrix product AB as a list of lists."""
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]
matrix_matrix_product_raw_source = """
def matrix_matrix_product(A, B):
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]

"""


# Problem 3
def prob3():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    #----------------------------------------------------------
    #MAKE A LIST OF SIZES WE WANT TO TEST
    #----------------------------------------------------------
    
    #I think testing matrices up to size 256 is good
    #I will want tests to be more sparse the larger the numbers are
    #So, I will make the sequence geometric
    #2**8 = 256
    sizes = 2**np.arange(9)
    
    #---------------------------------------------------------
    #TEST ALL THE SIZES ON THE LIST AND RECORD THE TIMES
    #---------------------------------------------------------
    #make space to record all the times
    matrix_matrix_times = mmts = np.array([])
    matrix_vector_times = mvts = np.array([])
    
    #iterate through the list of sizes
    for s in sizes:
        #get the proper setup for timeit
        #unfortunately, the """ string doesn't play well with tabs
	proper_setup = "from random import random\n"
        proper_setup += random_matrix_raw_source
        proper_setup += matrix_matrix_product_raw_source
        #generate 2 random matrices of this size
        proper_setup += "A = random_matrix(%d)\n"%s
        proper_setup += "B = random_matrix(%d)\n"%s
        
        #time the matrix-matrix multiplication 5 times, then append the average
        t = timeit("matrix_matrix_product(A, B)", setup=proper_setup, number=5)/5
        mmts = np.append(mmts, t)
        matrix_matrix_times = mmts
        
        #get the proper setup for timeit again
	proper_setup = "from random import random\n"
        proper_setup += matrix_vector_product_raw_source
        proper_setup += random_matrix_raw_source
        proper_setup += random_vector_raw_source
        proper_setup +="A = random_matrix(%d)\n"%s
        proper_setup +="x = random_vector(%d)"%s

        #time the matrix_vector multiplication 5 times, then append the average
        t = timeit("matrix_vector_product(A,x)", setup= proper_setup, number=5)/5
        mvts = np.append(mvts, t)
        matrix_vector_times = mvts

    #---------------------------------------------------------
    #PLOT THE SIZES VS TIME
    #---------------------------------------------------------
    #Matrix-Matrix
    #Subplot
    plt.subplot(1,2,2)
    plt.plot(sizes, matrix_matrix_times, 'g.-')
    plt.title("Matrix-Matrix Multiplication")
    plt.xlabel("Size")
    plt.ylabel("Seconds")
    
    #Matrix-Vector
    #Subplot
    plt.subplot(1,2,1)
    plt.plot(sizes, matrix_vector_times, 'b.-')
    plt.title("Matrix-Vector Multiplication")
    plt.xlabel("Size")
    plt.ylabel("Seconds")
    
    #Finish up
    #And show
    plt.suptitle("How Size Affects Matrix Multiplication")
    plt.show()
    return


# Problem 4
def prob4():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    #--------------------------------------------------------------------
    #GATHER THE DATA
    #--------------------------------------------------------------------
    #determine the inputs.  Doing more calculations now, so only up to 128
    sizes = 2**np.arange(8)
    
    #make arrays of the times
    vector_ts = np.array([])
    np_vector_ts = np.array([])
    matrix_ts = np.array([])
    np_matrix_ts = np.array([])
    
    #iterate through the sizes
    for s in sizes:
        #naive matrix-vector
        proper_setup =  "from random import random\n"
        proper_setup += matrix_vector_product_raw_source
        proper_setup += random_matrix_raw_source
        proper_setup += random_vector_raw_source
        proper_setup += "A = random_matrix(%d)\n"%s
        proper_setup += "x = random_vector(%d)\n"%s
        #take the average of 5 tests
        t = timeit("matrix_vector_product(A,x)", setup = proper_setup, number = 5)/5
        vector_ts = np.append(vector_ts, t)
        
        #np matrix_vector
        #put numpy import in the beginning instead of matrix_vector import
        x = matrix_vector_product_raw_source
        y = "import numpy as np"
        proper_setup = proper_setup.replace(x,y)
        #take average of 5 tests
        t = timeit("np.dot(A,x)", setup = proper_setup, number = 5)/5
        np_vector_ts = np.append(np_vector_ts, t)
        
        #np matrix-matrix
        #now we need matrix_matrix product
        proper_setup = proper_setup.replace("x = random_vector", "B = random_matrix")
        #we also no longer need the random_matrix_function
        proper_setup = proper_setup.replace(random_vector_raw_source,"")
        #take average of 5 tests
        t = timeit("np.dot(A,B)", setup = proper_setup, number = 5)/5
        np_matrix_ts = np.append(np_matrix_ts, t)
        
        #naive matrix_matrix
        #now we need the matrix_matrix_product function, not numpy
        x = "import numpy as np"
        y = matrix_matrix_product_raw_source
        proper_setup = proper_setup.replace(x,y)
        #take an average of 5 tests
        t = timeit("matrix_matrix_product(A,B)", setup=proper_setup, number=5)/5
        matrix_ts = np.append(matrix_ts, t)
    
    #--------------------------------------------------------------------
    #SHOW THE DATA
    #--------------------------------------------------------------------
    #Linear Subplot
    plt.subplot(1,2,1)
    plt.plot(sizes, vector_ts, 'bo-', label="Naive Matrix-Vector")
    plt.plot(sizes, np_vector_ts, 'go-', label="Numpy Matrix-Vector")
    plt.plot(sizes, matrix_ts, 'ro-', label="Naive Matrix-Matrix")
    plt.plot(sizes, np_matrix_ts, 'ko-', label="Numpy Matrix_Matrix")
    plt.legend(loc="upper left", prop={'size':9})
    plt.ylabel("Seconds")
    plt.xlabel("Size")
    
    #Logarithmic Subplot
    plt.subplot(1,2,2)
    plt.loglog(sizes, vector_ts, 'bo-', basex=2, basey=2, label="Naive Matrix-Vector")
    plt.loglog(sizes, np_vector_ts, 'go-', basex=2, basey=2, label="Numpy Matrix-Vector")
    plt.loglog(sizes, matrix_ts, 'ro-', basex=2, basey=2, label="Naive Matrix-Matrix")
    plt.loglog(sizes, np_matrix_ts, 'ko-', basex=2, basey=2, label="Numpy Matrix-Matrix")
    plt.legend(loc="upper left", prop={'size':9})
    plt.ylabel("Seconds")
    plt.xlabel("Size")
    
    #Give a suptitle,
    #and show
    mytitle = "Comparing Numpy to Naive Multiplication\n"
    mytitle +="on Linear and Logarithmic Graphs"
    plt.suptitle(mytitle)
    plt.show()
    return
    
    
#------------------------------------------------------------------------
#Testing
#------------------------------------------------------------------------
horse_array = np.load("horse.npy")

def compare(original, transformed, transformed_title):

    #find the smallest square bounds that will fit both
    lbound = min(min(original.ravel()), min(transformed.ravel()) )
    ubound = max(max(original.ravel()), max(transformed.ravel()) )
    
    #plot the original in one subplot
    plt.subplot(1,2,1)
    plt.plot(original[0], original[1], 'k,')
    plt.title("Original", fontsize = 18)
    
    #set our axes
    plt.axis([lbound, ubound, lbound, ubound])
    
    #make it square
    plt.gca().set_aspect("equal")
    
    #plot the transformed in the other subplot
    plt.subplot(1,2,2)
    plt.plot(transformed[0],transformed[1], 'k,')
    plt.title(transformed_title, fontsize = 18)
    
    #set our axes
    plt.axis([lbound, ubound, lbound, ubound])
    
    #make it square
    plt.gca().set_aspect("equal")
    
    plt.show()
    

def test_stretch():

    #stretch with a = .5
    stretched_horse = stretch(horse_array, .5, 1)
    compare(horse_array, stretched_horse, "Stretched .5 Along the X Axis")
    
    #stretch with b = 6/5
    stretched_horse = stretch(horse_array, 1, 6.0/5.0)
    compare(horse_array, stretched_horse, "Stretched 6/5 Along the Y Axis")
    
    #stretch with both
    stretched_horse = stretch(horse_array, .5, 6.0/5.0)
    compare(horse_array, stretched_horse, "Stretched 6/5 Along the Y axis\n and .5 along the X Axis")
    
def test_shear():
    
    #horizontal shear with a=1/2
    sheared_horse = shear(horse_array, .5, 0)
    compare(horse_array, sheared_horse, "Shear with a = 1/2")

def test_reflection():
    #reflection around y axis (a=0, b=1)
    a = 0
    b = 1
    reflected_horse = reflect(horse_array, a, b)
    title = "Reflection Around y axis (a=%d, b=%d)"%(a,b)
    compare(horse_array, reflected_horse, title)
    
    #more reflections
    a = 1
    b = 1
    reflected_horse = reflect(horse_array, a, b)
    title = "(a=%d, b=%d)"%(a,b)
    compare(horse_array, reflected_horse, title)

def test_rotation():
    rotated_horse = rotate(horse_array, np.pi/3)
    compare(horse_array, rotated_horse, "Rotate by pi/3")
    
    
if __name__ == "__main__":
    prob3()
    prob4()
