# linear_systems.py
"""Volume 1A: Linear Systems.
Logan Schelly
Math 345
1 October 2016
"""
import numpy as np
from scipy import linalg as la
from scipy import sparse
from timeit import timeit
from matplotlib import pyplot as plt

# Problem 1
def ref(A):
	"""Reduce the square matrix A to REF. You may assume that A is invertible
	and that a 0 will never appear on the main diagonal. Avoid operating on
	entries that you know will be 0 before and after a row operation.
	"""
	# let n be the number of rows and columns in the matrix
	if A.shape[0] != A.shape[1]:
		raise ValueError("%r is not a square matrix."%A)
	#this really only works if A is passed in with float entries
	if A.dtype != np.float:
		raise TypeError("This function only works on matrices with float entries")
		
	#we will operate directly on A
	n = A.shape[0]
	#nevermind
	refA = A.copy()
	
	#start at the first row and stop at the nth row
	for r in range(n):
		# zero out all the entries before the leading entry of the current row
		for x in range(r+1, n):
			#take the leading entry in row r
			ler = refA[r][r]
			#take the leading entry in row x
			lex = refA[x][r]
			#add -lex/ler * row r to row x
			refA[x][r::] -= lex/ler*refA[r][r::]
	#A should be row reduced.
	return refA



# Problem 2
def lu(A):
	"""Compute the LU decomposition of the square matrix A. You may assume the
	decomposition exists and requires no row swaps.

	Returns:
		L ((n,n) ndarray): The lower-triangular part of the decomposition.
		U ((n,n) ndarray): The upper-triangular part of the decomposition.
	"""
	# let n be the number of rows and columns in the matrix
	if A.shape[0] != A.shape[1]:
		raise ValueError("%r is not a square matrix."%A)
	#this really only works if A is passed in with float entries
	if A.dtype != np.float:
		raise TypeError("This function only works on matrices with float entries")
		
	m, n = A.shape
	U = A.copy()
	L = np.eye(m)
	for j in range (n):
		for i in range(j+1, m):
			L[i][j] = U[i][j] / U[j][j]
			U[i][j:] = U[i][j:] - L[i][j] * U[j][j:]
	return L, U


# Problem 3
def solve(A, b):
	"""Use the LU decomposition and back substitution to solve the linear
	system Ax = b. You may assume that A is invertible (hence square).
	"""
	#Find the LU decomposition of A
	L,U = lu(A)
	#Solve the system Ly = b
	#number_of_columns_of_A = A.shape[1].  
	#y should have as many entries as the columns of A
	y = np.full(A.shape[1], 0.0)
	#go through each entry in y and set it to the correct value
	"""
	print "this is L:\n", L
	"""
	for i in range(len(y)):
		#uncomment to have it display what it's doing.
		#numpy makes it so we can take a shortcut.
		"""
		print "Calculating y[%s] this way:\n"%i
		stuff_to_print = "y[%s] = b[%s]"%(i,i)
		subs = "y[%s] = %f"%(i, b[i])
		for j in range(i):
			stuff_to_print += " - L[%s][%s] * y[%s]"%(i,j,j)
			subs += " - %f * %f"%(L[i][j], y[j])
		print stuff_to_print
		print subs
		"""
		y[i] = b[i]
		#subtract out each l_{ix} * y_x where x<i
		y[i] -= sum(L[i][:i] * y[:i])
		"""
		print "This is now y[%d]: "%i , y[i], "\n"
		"""
	
	#Solve the system Ux = y
	x = y.copy() # need array same size as y
	"""
	print "This is U:\n", U
	print "This is y:", y
	"""
	
	for i in range(len(y))[::-1]: #back substitution starts at x[n]
		"""
		print "\nsolving for x[%s]"%i
		message = "U[%s][%s]*x[%s] = y[%s]"%(i,i,i,i)
		sub = "%f * x[%s] = %f"%(U[i][i], i, y[i])
		for j in range(i+1,len(y)):
			message += " - U[%s][%s]*x[%s]"%(i,j,j)
			sub += " - %f * %f"%(U[i][j], x[i])
		print message
		print sub
		
		Bug was that I was starting the index at i instead of i+1
		"""
		#Uii is the coefficient on x[i], so we divide by it
		#U[i][i:] is the coefficients for x[i:]
		x[i] = 1.0/U[i][i] * (y[i] - sum(U[i][i+1:]*x[i+1:]))
	return x


# Problem 4
def prob4():
	"""Time different scipy.linalg functions for solving square linear systems.
	Plot the system size versus the execution times. Use log scales if needed.
	"""
	n = 11
	sizes = 2**np.arange(1,n+1)
	
	#---------------------------------------------
	#Gather the Data
	#---------------------------------------------
	
	mysetup = "import numpy as np\n"
	mysetup += "from scipy import linalg as la\n"
	mysetup += "A = np.random.random((s,s))\n"
	mysetup += "b = np.random.random(s)"
	
	inv_times = list()
	solve_times = list()
	lufs_times = list()
	lus_times = list()
	
	for s in sizes:
		initialize_s = "s = %d\n"%s
		
		#invert A and left multiply to b.
		command = "np.dot( la.inv(A) , b)"
		inv_times.append(timeit(command, setup = initialize_s + mysetup,number = 5)/5.0)
		
		#la.solve()
		command = "la.solve(A,b)"
	 	solve_times.append(timeit(command, setup = initialize_s + mysetup,number = 5)/5.0)
	 	
	 	#la.lu_factor, then la.lu_solve
	 	command = "la.lu_solve( la.lu_factor(A) ,  b)"
	 	lufs_times.append(timeit(command, setup = initialize_s + mysetup, number = 5)/5.0)
	 	
	 	#la.lu_solve, with la.lu_factor already done
	 	extra_setup = "\nLU = la.lu_factor(A)"
	 	special_setup = initialize_s + mysetup + extra_setup
	 	command = "la.lu_solve(LU, b)"
	 	lus_times.append(timeit(command, setup = special_setup, number = 5)/5.0)
	 	
	 #------------------------------------------------
	 #Plot the Data
	 #------------------------------------------------
	plt.loglog(sizes, inv_times, 'go-', basex=2, basey=2 , label = "np.dot( la.inv(A), b)")
	plt.loglog(sizes, solve_times, 'ro-', basex=2, basey=2, label = "la.solve(A,b)")
	plt.loglog(sizes, lufs_times, 'bo-', basex=2, basey=2, label ="LU Factorization & Solve")
	plt.loglog(sizes, lus_times, 'ko-', basex=2, basey=2, label = "Only LU solve")
	 
	plt.title("Times for Techniques of Solving the System Ax = b")
	plt.xlabel("Size of the matrix -- n")
	plt.ylabel("Time")
	plt.legend(loc = "upper left")
	 	
	plt.show()
	return


# Problem 5
def prob5(n):
	"""Return a sparse n x n tridiagonal matrix with 2's along the main
	diagonal and -1's along the first sub- and super-diagonals.
	"""
	offsets = [-1,0,1]
	return sparse.diags([-1,2,-1], offsets, (n,n))

prob5_as_text = """
def prob5(n):
	offsets = [-1,0,1]
	return sparse.diags([-1,2,-1], offsets, (n,n))
	
"""

# Problem 6
def prob6():
	"""Time regular and sparse linear system solvers. Plot the system size
	versus the execution times. As always, use log scales where appropriate.
	"""
	n = 12
	sizes = 2**np.arange(1,n)
	
	reg_times = list()
	sparse_times = list()
	
	mysetup =  "import numpy as np\n"
	mysetup += "import scipy\n"
	mysetup += "from scipy import sparse\n"
	mysetup += "from scipy.sparse import linalg\n"
	mysetup +=  prob5_as_text
	
	for s in sizes:
		tests = 5
		b_setup = "b = np.random.random(%d)\n"%s
		Anp_setup = "A = prob5(%d).toarray()\n"%s
		Asp_setup = "A = prob5(%d).tocsr()\n"%s
		#test scipy.sparse.solve
		command = "scipy.sparse.linalg.spsolve(A,b)"
		t = timeit(command, setup = mysetup+b_setup+Asp_setup, number = tests)/tests
		sparse_times.append(t)
		#test scipy.solve
		command = "scipy.linalg.solve(A,b)"
		t = timeit(command, setup = mysetup+b_setup+Anp_setup, number = tests)/tests
		reg_times.append(t)
		
	#Plot the data
	
	plt.loglog(sizes, reg_times, 'ro-', basex=2, basey=2, label="Regular Solving")
	plt.loglog(sizes, sparse_times, 'bo-', basex=2, basey=2, label="Sparse Solving")
	
	plt.title("Comparing Scipy's Regular Solving and Sparse Solving Methods")
	plt.ylabel("Time")
	plt.xlabel("Size of Matrix")
	plt.legend(loc="upper left")
	
	plt.show()
	
	return


if __name__ == "__main__":
	want_to_test_prob1 = True
	want_to_test_prob4 = False
	want_to_test_prob6 = False
	
	if want_to_test_prob1:
		for n in range(2,6):
			A = np.random.random((n,n))
			print "\n\nThis is A:"
			print A
			print "\nTrying ref(A)"
			refA = ref(A)
			print "This is now A:"
			print A
			print "\nThis is ref(A)"
			print refA
			
	if want_to_test_prob4:
		prob4()
	if want_to_test_prob6:
		prob6()
