# qr_decomposition.py
"""Volume 1A: QR 1 (Decomposition).
Logan Schelly
Math 345
27 October
"""

import numpy as np
from scipy import linalg as la


# Problem 1
def qr_gram_schmidt(A):
	"""Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

	Inputs:
		A ((m,n) ndarray): A matrix of rank n.

	Returns:
		Q ((m,n) ndarray): An orthonormal matrix.
		R ((n,n) ndarray): An upper triangular matrix.
	"""
	m,n = A.shape
	Q = A.copy()
	R = np.zeros((n,n))
	for i in range(n):
		R[i,i] = la.norm(Q[:, i]) #norm of the ith column of A
		Q[:, i] /= R[i,i] #normalize
		for j in range(i+1, n):
			R[i,j] = np.dot(Q[:,j], Q[:,i]) #store the scalar projection
			Q[:,j] -= R[i,j]*Q[:,i] #orthogonalize all j columns
	return Q,R


# Problem 2
def abs_det(A):
	"""Use the QR decomposition to efficiently compute the absolute value of
	the determinant of A.

	Inputs:
		A ((n,n) ndarray): A square matrix.

	Returns:
		(float) the absolute value of the detetminant of A.
	"""
	Q,R = la.qr(A)
	return abs(R.diagonal().prod()) #absolute value of product of the diagonals of R


# Problem 3
def solve(A, b):
	"""Use the QR decomposition to efficiently solve the system Ax = b.

	Inputs:
		A ((n,n) ndarray): An invertible matrix.
		b ((n, ) ndarray): A vector of length n.

	Returns:
		x ((n, ) ndarray): The solution to the system Ax = b.
	"""
	n = A.shape[0]
	#compute Q and R
	Q,R = la.qr(A)
	
	#calculate y = Q^T b
	y = np.dot(Q.T, b)
	
	#solve Rx = y by back substitution
	#start with the last row and iterate backwards
	x = np.zeros_like(y)
	for i in range(n)[::-1]:
		#R[i,1]x[1] + R[i,2]x[2] + ... + R[i,n]x[n] = y[i]
		#R[i,i]x[i]=y[i]-(R[i,1]x[1]+...R[i,i-1]x[i-1] + R[i,i+1]x[i+1]...+R[i,n]x[n])
		#R is upper triangular
		#R[i,j] = 0 if i>j
		#R[i,i]x[i] = y[i]-(0*x[1]+...+0*x[i-1] + R[i,i+1]x[i+1]...+R[i,n]x[n])
		#R[i,i]x[i] = y[i] - (R[i,i+1]x[i+1] + R[i,i+2]x[i+2] + ... + R[i,n]x[n])
		#R[i,i]x[i] = y[i] - (R[i,i+1], ... , R[i,n])*dot*(x[i+1], ..., x[n])
		#R[i,i]x[i] = y[i] - (R[i, i+1:])*dot*(x[i+1:])
		#R[i,i]x[i] = y[i] - np.dot(R[i, i+1:],x[i+1:])
		x[i] = 1.0/R[i,i]*(y[i] - np.dot(R[i,i+1:], x[i+1:]))
	return x

def sign(x):
	if x >= 0:
		return 1
	else:
		return -1

# Problem 4
def qr_householder(A):
	"""Compute the full QR decomposition of A via Householder reflections.

	Inputs:
		A ((m,n) ndarray): A matrix of rank n.

	Returns:
		Q ((m,m) ndarray): An orthonormal matrix.
		R ((m,n) ndarray): An upper triangular matrix.
	"""	
	m,n  = A.shape
	R = A.copy()
	Q = np.eye(m)
	for k in range(n):
		u = R[k:, k].copy()
		u[0] += sign(u[0])*la.norm(u)
		u /= la.norm(u)
		R[k:,k:] -= 2 * np.outer(u, np.dot(u.T, R[k:,k:]))
		Q[k:,:] -= 2 * np.outer(u, np.dot(u.T, Q[k:,:]))
	return Q.T, R

# Problem 5
def hessenberg(A):
	"""Compute the Hessenberg form H of A, along with the orthonormal matrix Q
	such that A = QHQ^T.

	Inputs:
		A ((n,n) ndarray): An invertible matrix.

	Returns:
		H ((n,n) ndarray): The upper Hessenberg form of A.
		Q ((n,n) ndarray): An orthonormal matrix.
	"""
	m,n = A.shape
	H = A.copy()
	Q = np.eye(m)
	for k in range(0, n-2):
		u = H[k+1:,k].copy()
		u[0] += sign(u[0])*la.norm(u)
		u /= la.norm(u)
		H[k+1:, k:] -= 2 * np.outer(u, np.dot(u.T, H[k+1:, k:]))
		huh = H[:,k+1:]
		what = np.dot(huh, u)
		H[:,k+1:] -= 2 * np.outer(what , u.T)
		Q[k+1:,:] -= 2 * np.outer(u, np.dot(u.T, Q[k+1:, :]))
	return H, Q.T

if __name__ == "__main__":
	want_to_test_prob1 = False
	want_to_test_prob2 = False
	want_to_test_prob3 = False
	want_to_test_prob4 = False
	want_to_test_prob5 = True
	
	if want_to_test_prob1:
		n = 5 
		for m in range(5, 11):
			A = np.random.random((m, n))
			print "\n\nThis is A:"
			print A
			myQ, myR = qr_gram_schmidt(A)
			print "\nThis is my algorithms's Q and R:"
			print myQ
			print ""
			print myR
			print ""
			print "This is scipy's Q and R:"
			Q,R = la.qr(A, mode = "economic")
			print Q
			print "This is close to my Q:", np.allclose(myQ,Q)
			print
			print R
			print "This is close to my R:", np.allclose(myR,R)
			
			print "This is np.dot(Q,R):"
			maybe_A = np.dot(myQ, myR)
			print np.dot(myQ, myR)
			print "This is close to A:", np.allclose(maybe_A, A)
			print "\nnp.dot(myQ, myR) - A:"
			print A - maybe_A
			print "\nnp.dot(myQ.T, myQ):"
			print np.dot(myQ.T, myQ)
			print "This is close to np.eye(5,5):", (np.allclose(np.dot(myQ.T, myQ), np.eye(5,5)))
	
	if want_to_test_prob2:
		for n in range(1,15):
			A = np.random.random((n,n))
			mydet = abs_det(A)
			print mydet, "= abs_det(A)"
			det = np.linalg.det(A)
			print det, "= np.linalg.det(A)"
			print "np.allclose()? ", np.allclose(abs(det), mydet)
	
	if want_to_test_prob3:
		for n in range(1,15):
			A = np.random.random((n,n))
			b = np.random.random(n)
			my_x = solve(A,b)
			x = np.linalg.solve(A,b)
			print my_x, "== my x"
			print x, "== numpy's x"
			if x.all() == my_x.all():
				print "They're the same"
			elif np.allclose(x, my_x):
				print "They're pretty close"
			else:
				print "My x is wrong"
			print ""
			
	if want_to_test_prob4:
		for m in range(4,12):
			for n in range(4, m+1): #can't make the matrix fat, or columns dependent
				A =  np.random.random((m,n))
				print "\n\nThis is A:"
				print A
				myQ, myR = qr_householder(A)
				print "\nThis is my Q:"
				print myQ
				print "\nThis is my R:"
				print myR
				print "\nThis is QR:"
				myQR = np.dot(myQ,myR)
				print myQR
				if np.allclose(A, myQR):
					print "It's pretty close to A"
				else:
					print "It's not close to A"
					raise ValueError("Bad algorithm!")
				print "\nThis is np.dot(Q.T,Q):"
				maybe_I = np.dot(myQ.T, myQ)
				print maybe_I
				if np.allclose(maybe_I, np.eye(m)):
					print "my Q is pretty close to orthonormal"
				else:
					print "my Q is not orthonormal"
					raise ValueError("Bad algorithm!")
				Q,R = la.qr(A)
				print "\nThese are scipy's Q and R:"
				print Q
				print R
				
	if want_to_test_prob5:
		for throwaway_index in range(20):
			for n in range(3,12):
				for throwaway_index in range(3):
					A = np.random.random((n,n))
					print "\n\nThis is A"
					print A
					myH, myQ = hessenberg(A)
					print "\nThis is my H:"
					print myH
					if np.allclose(np.tril(myH, k=-2), np.zeros_like(myH)):
						print "H is pretty close to being upper hessenberg"
					else:
						raise ValueError("Bad Algorithm!  H sucked")
				
					print "\nThis is my Q:"
					print myQ
					if np.allclose(np.dot(myQ,myQ.T), np.eye(n)):
						print "Q is close to orthonormal"
					else:
						print np.dot(myQ,myQ.T)
						raise ValueError("Bad Algorithm! Q sucked")
					
					hopefully_A = np.dot(np.dot(myQ, myH), myQ.T)	
					if np.allclose(hopefully_A, A):
						print "QHQ^T is pretty close to A"
					else:
						raise ValueError("Bad Algorithm! QHQ^T is not close to A")
				
            
        
