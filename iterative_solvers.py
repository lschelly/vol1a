# iterative_solvers.py
"""Volume 1A: Iterative Solvers.
Logan Schelly
Math 345?
22 October 2016
"""
import numpy as np
from scipy import linalg as la
from scipy import sparse
from scipy.sparse import linalg
from matplotlib import pyplot as plt
from timeit import timeit

# Helper function
def diag_dom(n, num_entries=None):
	"""Generate a strictly diagonally dominant nxn matrix.

	Inputs:
		n (int): the dimension of the system.
		num_entries (int): the number of nonzero values. Defaults to n^(3/2)-n.

	Returns:
		A ((n,n) ndarray): An nxn strictly diagonally dominant matrix.
	"""
	if num_entries is None:
		num_entries = int(n**1.5) - n
	A = np.zeros((n,n))
	rows = np.random.choice(np.arange(0,n), size=num_entries)
	cols = np.random.choice(np.arange(0,n), size=num_entries)
	data = np.random.randint(-4, 4, size=num_entries)
	for i in xrange(num_entries):
		A[rows[i], cols[i]] = data[i]
	for i in xrange(n):
		A[i,i] = np.sum(np.abs(A[i])) + 1
	return A

diag_dom_raw_source = """
def diag_dom(n, num_entries=None):
	if num_entries is None:
		num_entries = int(n**1.5) - n
	A = np.zeros((n,n))
	rows = np.random.choice(np.arange(0,n), size=num_entries)
	cols = np.random.choice(np.arange(0,n), size=num_entries)
	data = np.random.randint(-4, 4, size=num_entries)
	for i in xrange(num_entries):
		A[rows[i], cols[i]] = data[i]
	for i in xrange(n):
		A[i,i] = np.sum(np.abs(A[i])) + 1
	return A

"""


# Problems 1 and 2
def jacobi_method(A, b, tol=1e-8, maxiters=100, plot=False):
	"""Calculate the solution to the system Ax = b voa the Jacobi Method.

	Inputs:
		A ((n,n) ndarray): A square matrix.
		b ((n,) ndarray): A vector of length n.
		tol (float, opt): the convergence tolerance.
		maxiters (int, opt): the maximum number of iterations to perform.
		plot (bool, opt): if True, plot the convergence rate of the algorithm.
			(this is for Problem 2).

	Returns:
		x ((n,) ndarray): the solution to system Ax = b.
	"""
	kth_x = np.zeros_like(b) #First guess is zero vector
	d = np.diag(A)
	
	if plot:
		errors = np.array([la.norm(np.dot(A,kth_x) - b, ord=np.inf)])
		
	for i in range(maxiters): #Iterate at most N times
	
		kp1th_x = kth_x + (b - np.dot(A, kth_x))/d #Use Equation 6.3
		#Plot if necessary
		if plot:
			this_error = la.norm(np.dot(A,kp1th_x) - b, ord=np.inf)
			errors = np.append(errors, this_error)
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
	
	if plot:
		kth_x = np.zeros_like(b)
		kp1th_x = None
		gauss_errors = np.array([])
		this_error = la.norm(np.dot(A,kth_x)- b, ord=np.inf)
		gauss_errors = np.append(gauss_errors, this_error)
		
		for i in range(maxiters):
			kp1th_x = kth_x.copy()
		
			for j in range(len(kp1th_x)):
				#formula 6.5
				kp1th_x[j] = kp1th_x[j] + 1/(A[j][j])*(b[j] - np.dot(A[j], kp1th_x))
		
			#Plot
			this_error = la.norm(np.dot(A,kp1th_x) - b, ord=np.inf)
			gauss_errors = np.append(gauss_errors, this_error)
			#Check to see if we've reached the error tolerance
			if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
				kth_x = kp1th_x
				break # reached error tolerance, exit loop
			else:
				kth_x = kp1th_x
				continue #did not reach error tolerance, continue with loop
		
		
		plt.semilogy(errors, 'go-', label = "Jacobi Method")
		plt.semilogy(gauss_errors, 'ro-', label = "Gauss-Seidel Method")
		plt.legend(loc = "upper right")
		plt.ylabel("Magnitude of Error")
		plt.xlabel("Iteration")
		plt.title("Convergence of Jacobi Method")
		plt.show()
		
	return kth_x


# Problem 3
def gauss_seidel(A, b, tol=1e-8, maxiters=100, plot=False):
	"""Calculate the solution to the system Ax = b via the Gauss-Seidel Method.

	Inputs:
		A ((n,n) ndarray): A square matrix.
		b ((n,) ndarray): A vector of length n.
		tol (float, opt): the convergence tolerance.
		maxiters (int, opt): the maximum number of iterations to perform.
		plot (bool, opt): if True, plot the convergence rate of the algorithm.

	Returns:
		x ((n,) ndarray): the solution to system Ax = b.
	"""
	kth_x = np.zeros_like(b) #square matrix, so solution is same size as b
	
	errors = np.array([])
	if plot:
		this_error = la.norm(np.dot(A,kth_x)- b, ord=np.inf)
		errors = np.append(errors, this_error)
	
	for i in range(maxiters):
		kp1th_x = kth_x.copy()
		
		for j in range(len(kp1th_x)):
			#formula 6.5
			kp1th_x[j] = kp1th_x[j] + 1/(A[j][j])*(b[j] - np.dot(A[j], kp1th_x))
		
		#Plot if necessary
		if plot:
			this_error = la.norm(np.dot(A,kp1th_x) - b, ord=np.inf)
			errors = np.append(errors, this_error)
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
	
	if plot:
		plt.semilogy(errors, 'go-', label = "||Ax - b||_inf")
		plt.legend(loc = "upper right")
		plt.ylabel("Magnitude of Error")
		plt.xlabel("Iteration")
		plt.title("Convergence of Gauss-Seidel Method")
		plt.show()
	
	return kth_x

gauss_seidel_raw_source = """
def gauss_seidel(A, b, tol=1e-8, maxiters=100, plot=False):

	kth_x = np.zeros_like(b) #square matrix, so solution is same size as b
	
	errors = np.array([])
	if plot:
		this_error = la.norm(np.dot(A,kth_x)- b, ord=np.inf)
		errors = np.append(errors, this_error)
	
	for i in range(maxiters):
		kp1th_x = kth_x.copy()
		
		for j in range(len(kp1th_x)):
			#formula 6.5
			kp1th_x[j] = kp1th_x[j] + 1/(A[j][j])*(b[j] - np.dot(A[j], kp1th_x))
		
		#Plot if necessary
		if plot:
			this_error = la.norm(np.dot(A,kp1th_x) - b, ord=np.inf)
			errors = np.append(errors, this_error)
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
	
	if plot:
		plt.semilogy(errors, 'go-', label = "||Ax - b||_inf")
		plt.legend(loc = "upper right")
		plt.ylabel("Magnitude of Error")
		plt.xlabel("Iteration")
		plt.title("Convergence of Gauss-Seidel Method")
		plt.show()
	
	return kth_x
	
"""

# Problem 4
def prob4():
	"""For a 5000 parameter system, compare the runtimes of the Gauss-Seidel
	method and la.solve(). Print an explanation of why Gauss-Seidel is so much
	faster.
	"""
	gauss_seidel_times = np.array([])
	scipy_times = np.array([])
	
	#for each n in {5,6,7 .. 11}
	for n in range(5,12):
		#generate a random 2^n by 2^n matrix A using diag_dom()
		A = diag_dom(2**n)
		#and a random 2^n vector b
		b = np.random.random(2**n)
		
		#Time how long it takes to solve the system with the Gauss-Seidel Method
		s = gauss_seidel_raw_source + diag_dom_raw_source
		s +="import numpy as np\n"
		s +="from scipy import linalg as la\n"
		s +="n = %d\n"%n
		s +="b = np.random.random(2**n)\n"
		s +="A = diag_dom(2**n)\n"
		t = timeit("gauss_seidel(A,b)", setup=s, number=5 )/5.0
		gauss_seidel_times = np.append(gauss_seidel_times, t)
		
		#Time how long it takes to solve with la.solve()
		s = s.replace(gauss_seidel_raw_source, "")
		t = timeit("la.solve(A,b)", setup=s, number=5)/5.0
		scipy_times = np.append(scipy_times, t)
		
	#plot the times versus system size
	sys_sizes = 2**(np.arange(5,12))
	plt.loglog(sys_sizes, gauss_seidel_times, 'ro-',basex=2, label="Gauss-Seidel")
	plt.loglog(sys_sizes, scipy_times, 'go-', basex=2, label="la.solve()")
	plt.xlabel("System Size (nxn)")
	plt.ylabel("Solve Time")
	plt.legend(loc = "upper left")
	plt.title("Scipy la.solve() vs. Gauss-Seidel")
	plt.show()
	
	print "Explaination: I have no idea why the Gauss-Seidel Method is faster, but I think it's because the Gauss Seidel Method is designed to work well with diagonal-dominant matrices, while la.solve() is designed to work well with any matrix.  So, la.solve() just doesn't have the homecourt advantage in this matchup."

# Problem 5
def sparse_gauss_seidel(A, b, tol=1e-8, maxiters=100):
	"""Calculate the solution to the sparse system Ax = b via the Gauss-Seidel
	Method.

	Inputs:
		A ((n,n) csr_matrix): An nxn sparse CSR matrix.
		b ((n,) ndarray): A vector of length n.
		tol (float, opt): the convergence tolerance.
		maxiters (int, opt): the maximum number of iterations to perform.

	Returns:
		x ((n,) ndarray): the solution to system Ax = b.
	"""
	kth_x = np.zeros_like(b) #First guess is zero vector
	
	for i in range(maxiters): #Iterate at most N times
		kp1th_x = kth_x.copy()
		
		for j in range(len(kp1th_x)):
			#formula 6.5
			rowstart = A.indptr[j]
			rowend = A.indptr[j+1]
			Ajx = np.dot(A.data[rowstart:rowend], kp1th_x[A.indices[rowstart:rowend]])
			kp1th_x[j] = kp1th_x[j] + 1/(A[j,j])*(b[j] - Ajx)
		
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
		
	return kth_x


# Problem 6
def sparse_sor(A, b, omega, tol=1e-8, maxiters=100):
	"""Calculate the solution to the system Ax = b via Successive Over-
	Relaxation.

	Inputs:
		A ((n,n) csr_matrix): An nxn sparse matrix.
		b ((n,) ndarray): A vector of length n.
		omega (float in [0,1]): The relaxation factor.
		tol (float, opt): the convergence tolerance.
		maxiters (int, opt): the maximum number of iterations to perform.

	Returns:
		x ((n,) ndarray): the solution to system Ax = b.
	"""
	kth_x = np.zeros_like(b) #First guess is zero vector
	
	for i in range(maxiters): #Iterate at most N times
		kp1th_x = kth_x.copy()
		
		for j in range(len(kp1th_x)):
			#formula 6.5
			rowstart = A.indptr[j]
			rowend = A.indptr[j+1]
			Ajx = np.dot(A.data[rowstart:rowend], kp1th_x[A.indices[rowstart:rowend]])
			kp1th_x[j] = kp1th_x[j] + omega/(A[j,j])*(b[j] - Ajx)
		
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
		
	return kth_x

sparse_sor_raw_source = """
def sparse_sor(A, b, omega, tol=1e-8, maxiters=100):
	kth_x = np.zeros_like(b) #First guess is zero vector
	
	for i in range(maxiters): #Iterate at most N times
		kp1th_x = kth_x.copy()
		
		for j in range(len(kp1th_x)):
			#formula 6.5
			rowstart = A.indptr[j]
			rowend = A.indptr[j+1]
			Ajx = np.dot(A.data[rowstart:rowend], kp1th_x[A.indices[rowstart:rowend]])
			kp1th_x[j] = kp1th_x[j] + omega/(A[j,j])*(b[j] - Ajx)
		
		#Check to see if we've reached the error tolerance
		if (la.norm(kth_x - kp1th_x, ord=np.inf) < tol):
			kth_x = kp1th_x
			break # reached error tolerance, exit loop
		else:
			kth_x = kp1th_x
			continue #did not reach error tolerance, continue with loop
		
	return kth_x
"""


# Problem 7
def finite_difference(n, debugging=False):
	"""Return the A and b described in the finite difference problem that
	solves Laplace's equation.
	"""
	#Build the diagonal block matrix B
	values = [1,-4,1]
	offsets = [-1,0,1]
	B = sparse.diags(values, offsets, shape = (n,n))
	if debugging:
		print "This is B:"
		print B.toarray()
		print "B's raw data:", B
		print "This is B's class:", B.__class__
	#Build the diagonal identity matrix I
	I = sparse.eye(n)
	if debugging:
		print "This is I:"
		print I.toarray()
		print "I's raw data:", I
		print "This is I's class:", I.__class__
	#Try to build a block-diagonal matrix from those diagonal matrices
	A = sparse.block_diag([B]*n)
	A.setdiag(1, -n)
	A.setdiag(1, n)
	if debugging:
		print "This is the array we will return:"
		print A.toarray()
		print "This is it's class:", A.__class__
	#Build the vector b
	#-100's happen at the first and last entries of a row
	#because the first and last ones touch the hot parts of the plate
	b = np.zeros(n**2)
	b[::n] = -100
	b[n-1::n] = -100 
	if debugging:
		print b
	return A,b

finite_difference_raw_source = """
def finite_difference(n, debugging=False):
	#Build the diagonal block matrix B
	values = [1,-4,1]
	offsets = [-1,0,1]
	B = sparse.diags(values, offsets, shape = (n,n))
	if debugging:
		print "This is B:"
		print B.toarray()
		print "B's raw data:", B
		print "This is B's class:", B.__class__
	#Build the diagonal identity matrix I
	I = sparse.eye(n)
	if debugging:
		print "This is I:"
		print I.toarray()
		print "I's raw data:", I
		print "This is I's class:", I.__class__
	#Try to build a block-diagonal matrix from those diagonal matrices
	A = sparse.block_diag([B]*n)
	A.setdiag(1, -n)
	A.setdiag(1, n)
	if debugging:
		print "This is the array we will return:"
		print A.toarray()
		print "This is it's class:", A.__class__
	#Build the vector b
	#-100's happen at the first and last entries of a row
	#because the first and last ones touch the hot parts of the plate
	b = np.zeros(n**2)
	b[::n] = -100
	b[n-1::n] = -100 
	if debugging:
		print b
	return A,b

"""

# Problem 8
def compare_omega():
	"""Time sparse_sor() with omega = 1, 1.05, 1.1, ..., 1.9, 1.95, tol=1e-2,
	and maxiters = 1000 using the A and b generated by finite_difference()
	with n = 20. Plot the times as a function of omega.
	"""
	omegas = np.arange(1,2,.05)
	n = 20
	tol = 1e-2
	maxiters = 1000
	times = list()
	for omega in omegas:
		mysetup = finite_difference_raw_source + sparse_sor_raw_source
		mysetup +="import numpy as np\n"
		mysetup +="from scipy import sparse\n"
		mysetup +="from scipy import linalg as la\n"
		mysetup +="A,b = finite_difference(%i)\n"%n
		command = "sparse_sor(A.tocsr(), b, %f, tol=%f, maxiters=%i)"%(omega, tol, maxiters)
		t = timeit(command, setup=mysetup, number=1)
		times.append(t)
	times = np.array(times)
	
	plt.plot(omegas, times, 'bo-')
	plt.xlabel("omega")
	plt.ylabel("solution time")
	plt.title("Problem 8")
	plt.show()
	return


# Problem 9
def hot_plate(n):
	"""Use finite_difference() to generate the system Au = b, then solve the
	system using SciPy's sparse system solver, scipy.sparse.linalg.spsolve().
	Visualize the solution using a heatmap using np.meshgrid() and
	plt.pcolormesh() ("seismic" is a good color map in this case).
	"""
	A,b = finite_difference(n)
	u = sparse.linalg.spsolve(A.tocsr(),b)
	u = u.reshape(n,n)
	
	X,Y = np.meshgrid(range(0,n+1), range(0,n+1))
	plt.pcolormesh(X,Y,u, cmap = "magma")
	plt.xlim(0,n)
	plt.ylim(0,n)
	plt.colorbar()
	plt.title("Heatmap with n = %d"%n)
	plt.show()
	return
	

if __name__ == "__main__":
	want_to_test_problem_1 = False
	want_to_test_problem_2 = True
	want_to_test_problem_3 = False
	want_to_test_problem_4 = False
	want_to_test_problem_5 = False
	want_to_test_problem_6 = False
	want_to_test_problem_7 = False
	want_to_test_problem_8 = False
	want_to_test_problem_9 = False

	if want_to_test_problem_1 or want_to_test_problem_2 or want_to_test_problem_3:
	
		methods_to_test = list()
		if want_to_test_problem_1 or want_to_test_problem_2:
			methods_to_test.append([jacobi_method, "Jacobi", want_to_test_problem_2])
		if want_to_test_problem_3:
			methods_to_test.append([gauss_seidel, "Gauss-Seidel", True])
			
		for m in methods_to_test:
			function = m[0]
			method_name = m[1]
			p = m[2]
			print "Testing the %s "%method_name + "method"
			
			for n in range(5,10):
				A = diag_dom(n)
				print "This is A:"
				print A
			
				b = np.random.random(n)
				print "This is b:"
				print b
			
				print "Using the %s method to solve Ax = b"%method_name
				x = function(A,b, plot = p)
				print "This is x:"
				print x
			
				print "Now, if we use la.solve(A,b) we get"
				print la.solve(A,b)
			
				print "\nNow we can test it on a random nxn matrix"
				A = np.random.random((n,n))
				print "How about this A?"
				print A
			
				print "Using the %s method to solve Ax = b"%method_name
				try:
					x = function(A,b, plot = p)
					print "This is x:"
					print x
			
					print "This is np.dot(A,x)"
					print np.dot(A,x)
					print "This is b:"
					print b
			
					if np.allclose(np.dot(A,x), b):
						print "The " + method_name + " method worked surprisingly well"
					else:
						print "The " + method_name + " sucked in this case."
				
				except ValueError:
					print "That didn't work"
				
				finally:
					print "\n"
				
	if want_to_test_problem_4:
		prob4()
		
	if want_to_test_problem_5:
		for dummy_index in range(3):
			A = diag_dom(5000)
			A_sparse = sparse.csr_matrix(A)
			b = np.random.random(5000)
			x = sparse_gauss_seidel(A_sparse,b)
			print "This is b:"
			print b
			print "This is Ax:"
			print np.dot(A,x)
			
	if want_to_test_problem_6:
		for omega in [.8, .6, 1.1]:
			print "This is the omega we will use:", omega
			A = diag_dom(5000)
			A_sparse = sparse.csr_matrix(A)
			b = np.random.random(5000)
			x = sparse_sor(A_sparse,b, omega)
			print "This is b:"
			print b
			print "This is Ax:"
			print np.dot(A,x)
	
	if want_to_test_problem_7:
		for n in range(2,6):
			A,b = finite_difference(n, True)
			print "Now change A to a CSR matrix"
			A = A.tocsr()
			print A
			print "A's class is: ", A.__class__
	
	if want_to_test_problem_8:
		compare_omega()
		
	if want_to_test_problem_9:
		for n in [10,100,500]:
			print "print doing " ,n
			hot_plate(n)
		
		
			
