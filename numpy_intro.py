# numpy_intro.py
"""Introductory Labs: Intro to NumPy.
Logan Schelly
Math 321
6 Sept 2016
"""

import numpy as np

def prob1():
    """Define the matrices A and B as arrays. Return the matrix product AB."""
    #don't forget double brakcets for 2 dimensions
    A = np.array([[3, -1, 4],[1, 5, -9]])
    B = np.array([[2, 6, -5, 3], [5, -8, 9, 7], [9, -3, -2, -3]])
    return np.dot(A,B)


def prob2():
    """Define the matrix A as an array. Return the matrix -A^3 + 9A^2 - 15A."""
    #note: A**2 squares each entry.  It does not compute np.dot(A,A)
    A = np.array( [ [3, 1, 4] , [1, 5, 9] , [-5, 3, 1] ] )
    return -( np.dot ( A , np.dot(A,A)) ) + 9*(np.dot(A,A)) - 15*A


def prob3():
    """Define the matrices A and B as arrays. Calculate the matrix product ABA,
    change its data type to np.int64, and return it.
    """
    #A was a 7x7 upper triangular matrix with only ones as entries
    A = np.triu(np.ones((7,7)))
    
    #B was a 7x7 matrix with 5's above the diagonal, and -1's on and underneath it
    #That is the sum of a 7x7 with only fives, and a 7x7 lower triangular with only
    #6's
    B = np.tril( -6 * np.ones((7,7)) ) + 5*np.ones((7,7))
    
    #calculate ABA
    ABA = np.dot( A , np.dot(B,A) )
    
    #change it's data type to int64
    ABA = np.array(ABA, dtype = np.int64)
    
    #Note, I'm not sure if they wanted this instead:
    #ABA.dtype = np.int64
    
    #return it
    return ABA
    
    #this is how you do it really quick:
    #return np.array(np.dot( np.triu(np.ones((7,7))) , np.dot(np.triu(np.ones((7,7))),np.tril( -6 * np.ones((7,7)) ) + 5*np.ones((7,7))) ))
    #but, of course, that's ridiculously hard to read

def prob4(A):
    """Make a copy of 'A' and set all negative entries of the copy to 0.
    Return the copy.

    Example:
        >>> A = np.array([-3,-1,3])
        >>> prob4(A)
        array([0, 0, 3])
    """
    #make a copy of the array
    copyA = A.copy()
    
    #another option is copyA = np.copy(A)
    
    #set all negative entries of the copy to 0
    negative_entries_mask = copyA < 0
    copyA[negative_entries_mask] = 0
    
    #alternatively: A[A<0] = 0
    
    #return the copy
    return copyA
    


def prob5():
    """Define the matrices A, B, and C as arrays. Return the block matrix
                                | 0 A^T I |
                                | A  0  0 |,
                                | B  0  C |
    where I is the identity matrix of appropriate size and each 0 is a matrix
    of all zeros, also of appropriate sizes.
    """
    """
    A = [0 2 4]
        [1 3 5]
    """
    A = np.array([[0,2,4] , [1,3,5]])
    """
    B = [3 0 0]
        [3 3 0]
        [3 3 3]
        a 3x3 lower triangular matrix of all 3's
    """
    B = np.tril( np.full( (3,3) , 3) )
    """
    C = [-2  0  0]
        [ 0 -2  0]
        [ 0  0 -2]
    """
    C = -2*np.eye(3,3)
    """
    Blk_Mtx = |A|
              |B|
    """
    Blk_Mtx = np.vstack((A,B))
    """
    Need to make the |0|
                     |0| to the right of where we are now
    
    It will have the same number of rows as Blk_Mtx and columns of A^T
    """
    number_of_columns = A.T.shape[1]
    number_of_rows = Blk_Mtx.shape[0]
    #double parentheses on this are important.  dimension is given as a tuple.
    zeroes_part = np.zeros((number_of_rows, number_of_columns))
    """
    Blk_Mtx = |A| hstacked with |0|
              |B|               |0|
    
    So, now Blk_Mtx is
    |A 0|
    |B 0|
    """
    Blk_Mtx = np.hstack((Blk_Mtx, zeroes_part))
    """
    Need |0|
         |C|
         0 will have same #rows as A and #columns of C
    """
    number_of_columns_C = C.shape[1]
    number_of_rows_A = A.shape[0]
    zero_matrix_above_C = np.zeros((number_of_rows_A, number_of_columns_C))
    zero_over_C_and_C = np.vstack(( zero_matrix_above_C, C ))
    """
    Blk_Mtx = |A 0| hstacked |0|
              |B 0| with     |C|
    """
    Blk_Mtx = np.hstack((Blk_Mtx, zero_over_C_and_C))
    """
    Now I need to make |0 A^T I|
    the Identity matrix will HAVE to be square, so it will be A.T.shape[0] by A.T.shape[0] 
    """
    I = np.eye(A.T.shape[0], A.T.shape[0])
    
    #now make |A^T I|
    Top_block = np.hstack((A.T, I))
    
    #make 0.  Rows will be same as top_block. Columns same as B
    Top_left_0 = np.zeros((Top_block.shape[0] , B.shape[1]))
    
    #Finish making |0 A^T I| (Top_block)
    Top_block = np.hstack(( Top_left_0 , Top_block ))
    
    """
    Blk_Mtx = |Top_block| = |0 A^T I|
              |Blk_Mtx  |   |A  0  0|
                            |B  0  C|
    """
    Blk_Mtx = np.vstack(( Top_block, Blk_Mtx ))
    
    
    return Blk_Mtx


def prob6(A):
    """Divide each row of 'A' by the row sum and return the resulting array.

    Example:
        >>> A = np.array([[1,1,0],[0,1,0],[1,1,1]])
        >>> prob6(A)
        array([[ 0.5       ,  0.5       ,  0.        ],
               [ 0.        ,  1.        ,  0.        ],
               [ 0.33333333,  0.33333333,  0.33333333]])
    """
    #make an array of the sums of each row
    sum_array = A.sum(axis = 1)
    
    #avoid any integer division by forcing it to be float
    sum_array = np.array(sum_array, dtype = np.float64)
    
    #Reshape it into a column vector
    #-1 tells it to make the rows correct for a 1 column matrix
    column_of_sums = sum_array.reshape(-1, 1)
    
    #divide each row of A by the sum of the row
    #implement by division broadcasting
    Stochastic_A = A / column_of_sums
    
    return Stochastic_A
    
    
    pass


def prob7():
    """Given the array stored in grid.npy, return the greatest product of four
    adjacent numbers in the same direction (up, down, left, right, or
    diagonally) in the grid.
    """
    #load in the grid as a big freakin' matrix
    grid = np.load("grid.npy")
    
    """
    Ok, let's consider the syntax
    grid[:, 0:4]
    That returns just the first 4 columns of the grid.
    
    So,
    grid[ : , 0:4].sum(axis=1)
    gives the sum of those first 4 columns
    
    max(grid[ : , 0:4].sum(axis=1)) gives the max entry
    
    So, how would we do that for each subsequent set of columns?
    
    Well, instead, think of this:
    
    grid[ : , 0:4] + grid[ : , 1:5] + grid[ : , 2:6] + grid[ : ,  3:7]
    
    gives 4 rows of columns that are the sums of the first 4 horizontal sums.
    max (grid[ : , 0:4] + grid[ : , 1:5] + grid[ : , 2:6] + grid[ : ,  3:7])
    gives the max of the first 4 columns we would find the interative way
    
    And, why limit ourselves to a 4 column matrix?  Why not do
    grid[ : ,  0:50] + grid [ : , 1:51] ... etc?
    
    In fact, ideally, we would just want the number of columns in our matrix
    to be only 3 less than the grid
    grid[ : , 0: len(grid[]-3)
    
    Technically, the syntax to find out the number of columns would be
    grid.shape[1]
    
    ...duh
    
    ...oh, and this is products, not sums.  
    So, I hope the * operator broadcasts in numpy
    """
    
    #use the acronym nis for " (amount of) numbers in sum"
    nis = 4
    
    #we usually say a matrix is mxn, so let n be the number of columns
    n = grid.shape[1]
    
    horizontal_max_prod = np.max(grid[:,0:n-nis+1] * grid[:,1:n-nis+2] * grid[:,2:n-nis+3] * grid[:,nis-1:n])
    
    #we should have a very similar case with vertical sums
    
    #matrices are mxn
    m = grid.shape[0]
    
    vertical_max_prod = np.max(grid[0:n-nis+1,:] * grid[1:n-nis+2,:] * grid[2,n-nis+3] * grid[nis-1:n,])
    
    """
    Ok, we've got all the straight-across products.  But what about the diagonals.
    
    Well, consider the matrix |a b c|
                              |d e f|
                              |g h i|
                              
    We can do a shift like this: |a b c|
                               |d e f|
                             |g h i|
    And now products that were to the right-down diagonal are now vertical!
    
    So... how can we use this?
    
    Well, perhaps I should just shift the rows individually.
    
    """
    r_d_diag_max_prod = np.max(grid[0:17, 0:17] * grid[1:18, 1:18] * grid[2:19, 2:19] * grid[3:20, 3:20])
    l_u_diag_max_prod = np.max(grid[3:20, 0:17] * grid[2:19, 1:18] * grid[1:18, 2:19] * grid[0:17, 3:20])
    
    #and now I just return the max of the 4 maxes I found
    return max(vertical_max_prod, horizontal_max_prod, r_d_diag_max_prod, l_u_diag_max_prod)
    
    #note to self: max is in l_u_diag matrix coordinates: 12, 3.  13th row 4th column
    #89*94*97*87.  In the original matrix, the coordinate is 7th column 13th rown. That is the 89.  the 94, 97 and 87 are the left downward diangonal.  convincing max.

if __name__ == "__main__":
    pass
